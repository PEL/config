
from bliss.setup_globals import *

load_script("env.py")

print("")
print("Welcome to your new 'env' BLISS session !! ")
print("")
print("You can now customize your 'env' session by changing files:")
print("   * /env_setup.py ")
print("   * /env.yml ")
print("   * /scripts/env.py ")
print("")