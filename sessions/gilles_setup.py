
from bliss.setup_globals import *

load_script("gilles.py")

print("")
print("Welcome to your new 'gilles' BLISS session !! ")
print("")
print("You can now customize your 'gilles' session by changing files:")
print("   * /gilles_setup.py ")
print("   * /gilles.yml ")
print("   * /scripts/gilles.py ")
print("")