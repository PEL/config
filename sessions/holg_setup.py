
from bliss.setup_globals import *

load_script("holg.py")

print("")
print("Welcome to the 'holg' BLISS session !! ")
print("")
# print("You can now customize your 'holg' session by changing files:")
# print("   * /holg_setup.py ")
# print("   * /holg.yml ")
# print("   * /scripts/holg.py ")
# print("")
