
from bliss.setup_globals import *

from bliss.common.standard import loopscan
from bliss.common.measurement import SoftCounter

from pel.PELscans import *
from pel.PELicepapTools import *

from time import sleep

load_script("id16hpz.py")

print("Welcome to your new 'id16hpz' BLISS session !! ")

# Do not decalre hpzrotencct in config-objects list in session .yml file
hpzrotid16_enc_ct = SoftCounter(hpzrotid16_enc, 'read', name='hpzrotid16_enc_ct')
hpzrotid16_axis_ct = SoftCounter(hpzrotid16_axis, 'read', name='hpzrotid16_axis_ct')

hpzrotid16_steps = icepap_pos_motor(hpzrotid16)
hpzrotid16_steps_ct = SoftCounter(hpzrotid16_steps, 'read', name='hpzrotid16_steps_ct')

