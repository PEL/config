
from bliss.setup_globals import *

load_script("id27dcm_bragg.py")

print("")
print("Welcome to your new 'id27dcm_bragg' BLISS session !! ")
print("")
print("You can now customize your 'id27dcm_bragg' session by changing files:")
print("   * /id27dcm_bragg_setup.py ")
print("   * /id27dcm_bragg.yml ")
print("   * /scripts/id27dcm_bragg.py ")
print("")