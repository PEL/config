
from bliss.setup_globals import *

load_script("id27dcm_gap.py")

print("")
print("Welcome to your new 'id27dcm_gap' BLISS session !! ")
print("")
print("You can now customize your 'id27dcm_gap' session by changing files:")
print("   * /id27dcm_gap_setup.py ")
print("   * /id27dcm_gap.yml ")
print("   * /scripts/id27dcm_gap.py ")
print("")