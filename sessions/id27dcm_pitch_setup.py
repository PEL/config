
from bliss.setup_globals import *

load_script("id27dcm_pitch.py")

print("")
print("Welcome to your new 'id27dcm_pitch' BLISS session !! ")
print("")
print("You can now customize your 'id27dcm_pitch' session by changing files:")
print("   * /id27dcm_pitch_setup.py ")
print("   * /id27dcm_pitch.yml ")
print("   * /scripts/id27dcm_pitch.py ")
print("")