
from bliss.setup_globals import *

load_script("id27dcm_roll.py")

print("")
print("Welcome to your new 'id27dcm_roll' BLISS session !! ")
print("")
print("You can now customize your 'id27dcm_roll' session by changing files:")
print("   * /id27dcm_roll_setup.py ")
print("   * /id27dcm_roll.yml ")
print("   * /scripts/id27dcm_roll.py ")
print("")