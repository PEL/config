from bliss.common.tango import DeviceProxy

HexaSpx = DeviceProxy('nano:20000/ID16NI/emotion_spx/spx')
spx = SoftAxis('Position', HexaSpx)

HexaSpy = DeviceProxy('nano:20000/ID16NI/emotion_sp/spy')
spy = SoftAxis('Position', HexaSpy)

HexaSpz = DeviceProxy('nano:20000/ID16NI/emotion_spz/spz')
spz = SoftAxis('Position', HexaSpz)

HexaSprx = DeviceProxy('nano:20000/ID16NI/emotion_sprx/sprx')
sprx = SoftAxis('Position', HexaSprx)

HexaSpry = DeviceProxy('nano:20000/ID16NI/emotion_spry/spry')
spry = SoftAxis('Position', HexaSpry)


def change_mode_id16_count():
    
    print("change ID16 counters mode to SINGLE")

#    hpzrotid16_enc_ct.mode=4
#    hpzrotid16_axis_ct =4

    adc01.mode=4
    adc02.mode=4
    adc03.mode=4
    adc04.mode=4
    adc05.mode=4
    adc06.mode=4
    adc07.mode=4
    adc08.mode=4
    adc09.mode=4
    adc10.mode=4
    adc11.mode=4
    adc12.mode=4
    acqtime.mode=4


    met_Rx.mode=4
    met_Ry.mode=4
    met_Tx.mode=4
    met_Ty.mode=4
    met_Tz.mode=4

    err_Rx.mode=4
    err_Ry.mode=4
    err_Tx.mode=4
    err_Ty.mode=4
    err_Tz.mode=4

    p1_measured_pos.mode=4
    p2_measured_pos.mode=4
    p3_measured_pos.mode=4
    p4_measured_pos.mode=4
    p5_measured_pos.mode=4
    p6_measured_pos.mode=4
    
    spx_pos.mode=4
    spy_pos.mode=4
    spz_pos.mode=4
    sprx_pos.mode=4
    spry_pos.mode=4
    
def wmhpz():
    
    print("Positions HPZ")
    print(spx.position,spy.position,spz.position,sprx.position,spry.position)

def fogale_on_the_fly():
    narco = DeviceProxy("//nano:20000/id16ni/hpz/adcsraw")
    
    deltat = 30
    waitt = 0.5
    vel_list = [0.05, 0.1, 0.2, 0.5, 1, 2, 5, 10]
    
    vel = 0
    file_name = "/tmp/adcsraw/fogaleopen_speed%02dp%02d_acc4p0_iceclosed_forward_%ds.txt" % (vel, 100*(vel-int(vel)), deltat)
    print(file_name)
    narco.start(file_name);sleep(deltat);narco.stop()
    file_name = "/tmp/adcsraw/fogaleopen_speed%02dp%02d_acc4p0_iceclosed_backward_%ds.txt" % (vel, 100*(vel-int(vel)), deltat)
    print(file_name)
    narco.start(file_name);sleep(deltat);narco.stop()
    for vel in vel_list:
        hpzrotid16.velocity = vel
        pos_end = (deltat+2*waitt)*vel+vel*vel/hpzrotid16.acceleration
        print(vel)
        print(hpzrotid16.velocity)
        print(pos_end)
        file_name = "/tmp/adcsraw/fogaleopen_speed%02dp%02d_acc4p0_iceclosed_forward_%ds.txt" % (vel, 100*(vel-int(vel)), deltat)
        print(file_name)
        move(hpzrotid16, pos_end, wait=False);sleep(hpzrotid16.acctime+waitt);narco.start(file_name);sleep(deltat);narco.stop()
        sleep(hpzrotid16.acctime+waitt*10) # should be waitmove like
        file_name = "/tmp/adcsraw/fogaleopen_speed%02dp%02d_acc4p0_iceclosed_backward_%ds.txt" % (vel, 100*(vel-int(vel)), deltat)
        print(file_name)
        move(hpzrotid16, 0, wait=False);sleep(hpzrotid16.acctime+waitt);narco.start(file_name);sleep(deltat);narco.stop()
        sleep(hpzrotid16.acctime+waitt*10) # should be waitmove like
    hpzrotid16.velocity = 2
