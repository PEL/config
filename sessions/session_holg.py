
from bliss.setup_globals import *


# the file located in ~/local/beamline_configuration/scripts
load_script("session_holg.py")

print("")
print("Welcome to the 'session_holg' BLISS session !! ")
print("")
print("You can now customize your 'session_holg' session by changing files:")
print("   * /session_holg_setup.py ")
print("   * /session_holg.yml ")
print("   * /scripts/session_holg.py ")
print("")

print("\nsession_holg: Executing configuration...")

config.reload()
#ApplySessionAxisConfig()

