
from bliss.setup_globals import *

load_script("session_test.py")

print("")
print("Welcome to your new 'session_test' BLISS session !! ")
print("")
print("You can now customize your 'session_test' session by changing files:")
print("   * /session_test_setup.py ")
print("   * /session_test.yml ")
print("   * /scripts/session_test.py ")
print("")